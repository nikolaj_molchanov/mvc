<?
/*Создание поведения USER*/
class UserController implements IController{
	private $_fc, $_model;
		
	function __construct(){
		$this->_fc = FrontController::getInstance();
		/* Инициализация модели */
		$this->_model = new FileModel();
	}
	/*чтения данных из файла*/
	protected function getFile($user_file){
		return unserialize(file_get_contents($user_file));
	}
		
	public function helloAction(){
		$params = $this->_fc->getParams();
		$this->_model->name = $params['name'];
		
		/* подключение VIEW файла */
		$output = $this->_model->render(USER_DEFAULT_FILE);
		
		/* представление результата */
		$this->_fc->setBody($output);
	}
	
	/*Создание действия и представления для вывода списка пользователей*/
	public function listAction(){
		$this->_model->list = array_keys($this->getFile(USER_DB));
		
		/* подключение VIEW файла */
		$output = $this->_model->render(USER_LIST_FILE);
		
		/* представление результата */
		$this->_fc->setBody($output);
	}
	
	/*Создание действия и представления для вывода роли пользователя*/
	public function getAction(){
		$params = $this->_fc->getParams(); 
		$this->_model->name = $params['role']; //Получение имени пользователя
		
		$this->_model->user = $this->getFile(USER_DB);
		
		/* подключение VIEW файла */
		$output = $this->_model->render(USER_ROLE_FILE);
		
		/* представление результата */
		$this->_fc->setBody($output);
	}
	/*Добавления пользователя  и его роль в список с представлением*/
	public function addAction(){
		$params = $this->_fc->getParams(); //ассоциативный массив с именем и ролью пользователя
		$name = $params['name']; //Получение имени пользователя
		$this->_model->name = $name;
		
		$user = $this->getFile(USER_DB);
		$user[$name] = $params['role']; //добавления в массив даных о пользователе
		$this->_model->user = $user;
		
		file_put_contents(USER_DB, serialize($user)); //запись нового списка в файл
		
		/* подключение VIEW файла */
		$output = $this->_model->render(USER_ADD_FILE);
		/* представление результата */
		$this->_fc->setBody($output);
	}

}
?>